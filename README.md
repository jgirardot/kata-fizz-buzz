## Step 1 :
Afficher les nombres de 1 à 100 et pour chacun :
- les nombres multiples de **3** deviennent **Fizz**
- les nombres multiples de **5** deviennent **Buzz**
- les nombres multiples de **3** et **5** deviennent **FizzBuzz**

## Step 2 :
- les nombres multiples de **7** deviennent **Wizz**

## Step 3 :
- les nombres multiples de **3** OU qui contiennent le chiffre **3** deviennent **Fizz**
- les nombres multiples de **5** OU qui contiennent le chiffre **5** deviennent **Buzz**


