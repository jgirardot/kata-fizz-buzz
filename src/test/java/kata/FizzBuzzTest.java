package kata;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	@Test
	void fizz_buzz_to_five() {
		// Given

		// When
		List<String> fizzBuzzResult = FizzBuzz.fizzBuzz(5);

		// Then
		assertThat(fizzBuzzResult)
				.isEqualTo(List.of("1", "2", "Fizz", "4", "Buzz"));
	}
}